var mainData = [];
var newtodo = function () {
    document.getElementById("new-todo").innerHTML = "  Enter Name of new Todo : <input type=\"text\" id=\"name\"> <br> \n  Enter Date of new Todo :   <input type=\"text\" id=\"date\"> <br><button onclick=\"newtodoback()\">Done</button>";
};
var newtodoback = function () {
    var name = document.getElementById("name").value;
    var date = document.getElementById("date").value;
    fetch("http://localhost:8000/new/" + name + "/" + date).then(function (res) {
        init();
    });
    document.getElementById("new-todo").innerHTML = "";
};
var removeTodo = function () {
    document.getElementById("rm-todo").innerHTML = "Enter index of todo to be removed 0.." + mainData.length + "  :  <input type=\"text\" id=\"index\"><br><button onclick=\"rmtodoback()\">Done</button>";
    //   var index = prompt("Enter index of todo to be removed 0.." + mainData.length);
    //   if (index != null) {
    //     var indexi = parseInt(index);
    //     if (indexi >= 0 && indexi < mainData.length) {
    //       fetch("http://localhost:8000/rm/" + indexi).then(function (res) {
    //         init();
    //       });
    //     }
    //   }
};
var rmtodoback = function () {
    var index = document.getElementById("index").value;
    if (index != null) {
        var indexi = parseInt(index);
        if (indexi >= 0 && indexi < mainData.length) {
            fetch("http://localhost:8000/rm/" + indexi).then(function (res) {
                init();
            });
        }
    }
};
var init = function () {
    var newdiv = document.createElement("div");
    newdiv.setAttribute("class", "todo");
    fetch("http://localhost:8000/init")
        .then(function (response) {
        // if (response.status === 200) {
        console.log("requested");
        return response.json();
        // }
    })
        .then(function (data) {
        mainData = data;
        for (var index = 0; index < data.length; index++) {
            var element = data[index];
            var nameDiv = document.createElement("div");
            var dateDiv = document.createElement("div");
            var indexDiv = document.createElement("div");
            var newTodoDiv = document.createElement("div");
            nameDiv.innerText = "Name : " + element.name;
            dateDiv.innerText = "Date : " + element.date;
            indexDiv.innerText = "Index :" + index;
            newTodoDiv.appendChild(indexDiv);
            newTodoDiv.appendChild(nameDiv);
            newTodoDiv.appendChild(dateDiv);
            newdiv.appendChild(newTodoDiv);
        }
    })["catch"](function (err) {
        console.log("Error :" + err);
        alert("Error " + err);
    });
    var root = document.getElementById("main");
    root.innerHTML = "";
    root.appendChild(newdiv);
};
init();
//# sourceMappingURL=index.js.map